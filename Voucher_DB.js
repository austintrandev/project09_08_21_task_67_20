/**
 * CƠ SỞ DỮ LIỆU QUẢN LÝ DANH SÁCH VOUCHER
*/

// Khởi tạo mảng gVoucherDb gán dữ liêu từ server trả về 
var gVoucherDb = {
  vouchers: [],

  // order methods
  // function get VOUCHER index form VOUCHER id
  // get VOUCHER index from VOUCHER id
  getIndexFromVoucherId: function (paramVoucherId) {
    var vVoucherIndex = -1;
    var vVoucherFound = false;
    var vLoopIndex = 0;
    while (!vVoucherFound && vLoopIndex < this.vouchers.length) {
      if (this.vouchers[vLoopIndex].id === paramVoucherId) {
        vVoucherIndex = vLoopIndex;
        vVoucherFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vVoucherIndex;
  },

  // hàm show order obj lên modal
  showVoucherDataToForm: function (paramRequsetData) {
   
    $("#inp-ma-voucher").val(paramRequsetData.maVoucher);
    $("#inp-phan-tram-giam-gia").val(paramRequsetData.phanTramGiamGia);
    $("#inp-ghi-chu").val(paramRequsetData.ghiChu);
    $("#inp-ngay-tao").val(paramRequsetData.ngayTao);
    $("#inp-ngay-cap-nhat").val(paramRequsetData.ngayCapNhat);
  },

  // hàm lấy ra đc id tiếp theo
  getNextId: function () {
    var vNextId = 0;
    if (this.orders.length == 0) {
      vNextId = 1;
    }
    else {
      vNextId = this.orders[this.orders.length - 1].id + 1;
    }
    return vNextId;
  },

  
};

